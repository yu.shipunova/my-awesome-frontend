import http from '../http-common'

class MedicalStaffDataService {
    getMedicalStaffs() {
        return http.get('/medical_staff')
    }

    get(id) {
        return http.get(`/medical_staff/${id}`)
    }

    add(data) {
        return http.post('/medical_staff', data)
    }

    update(id, data) {
        return http.put(`/medical_staff/${id}`, data)
    }

    delete(id) {
        return http.delete(`/medical_staff/${id}`)
    }
}

export default new MedicalStaffDataService()