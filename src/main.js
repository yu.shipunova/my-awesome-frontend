import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import VueRouter from "vue-router";
import router from "@/router";
import Global from './global'
import VueGoogleCharts from 'vue-google-charts'
import AccountantNavigation from "./pages/accountant/AccountantNavigation";
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import VueJwtDecode from 'vue-jwt-decode'
import Vuex from 'vuex'

Vue.config.productionTip = false;

Vue.use(vuetify, {
  iconfont: 'mdi'
});
Vue.use(VueRouter);
Vue.use(VueGoogleCharts)
Vue.use(VueJwtDecode)
Vue.use(Vuex)

new Vue({
  AccountantNavigation,
  router,
  vuetify,
  data:
  Global,
  render: h => h(App),
}).$mount('#app')
