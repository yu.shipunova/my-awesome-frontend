import VueRouter from 'vue-router'
import MedicalStaff from "@/pages/accountant/MedicalStaff";
import Division from "@/pages/accountant/Division";
import Service from "@/pages/accountant/Service";
import Drug from "@/pages/accountant/Drug";
import DiseaseHistory from "./pages/accountant/DiseaseHistory";
import MedicalStaffList from "./pages/medicalStaff/MedicalStaffList";
import DiseaseRecommendation from "./pages/medicalStaff/DiseaseRecommendation";
import PaymentOfTreatmentReport from "./pages/accountant/PaymentOfTreatmentReport";
import PatientDiseaseHistory from "./pages/accountant/PatientDiseaseHistory";
import PriceListReport from "./pages/medicalStaff/PriceListReport";
import DistributionPatientsByWard from "./pages/medicalStaff/DistributionPatientsByWard";
import PurposeOfTreatment from "./pages/medicalStaff/PurposeOfTreatment";
import DiseaseDynamicReport from "./pages/medicalStaff/DiseaseDynamicReport";
import StatementOfPaidServices from "./pages/accountant/StatementOfPaidServices";
import DrugRecommendation from "./pages/medicalStaff/DrugRecommendation";
import ReceiptReport from "./pages/medicalStaff/ReceiptReport";
import MedicalStaffHome from "./pages/medicalStaff/MedicalStaffHome";
import WardDiagram from "./pages/medicalStaff/WardDiagram";
import PriceArchive from "./pages/accountant/PriceArchive";
import AccountantDiagram from "./pages/accountant/AccountantDiagram";
import DynamicReport from "./pages/medicalStaff/DynamicReport";
import AccountantDynamicReport from "./pages/accountant/AccountantDynamicReport";
import ServiceAndDrugArchive from "./pages/medicalStaff/ServiceAndDrugArchive";
import ServiceMedication from "./pages/medicalStaff/ServiceMedication";
import DrugMedication from "./pages/medicalStaff/DrugMedication";
import PerfomingTreatment from "./pages/medicalStaff/PerfomingTreatment";
import App from "./App";

export default new VueRouter ({
    routes: [
        {
            path: '/medicalStaff_home',
            component: MedicalStaffHome,
            meta: { auth: true },
        },
        {
            path: '/medical_staff',
            component: MedicalStaff,
            meta: { auth: true },
        },
        {
            path: '/division',
            component: Division,
            meta: { auth: true },
        },
        {
            path: '/service',
            component: Service,
            meta: { auth: true },
        },
        {
            path: '/drug',
            component: Drug,
            meta: { auth: true },
        },
        {
            path: '/diseaseHistory',
            component: DiseaseHistory,
            meta: { auth: true },
        },
        {
            path: '/patient_disease_history/:id',
            component: PatientDiseaseHistory,
            meta: { auth: true },
        },
        {
            path: '/patient_service_medication/:id',
            component: ServiceMedication,
            meta: { auth: true },
        },
        {
            path: '/patient_drug_medication/:id',
            component: DrugMedication,
            meta: { auth: true },
        },
        {
            path: '/medical_staff_list',
            component: MedicalStaffList,
            meta: { auth: true },
        },
        {
            path: '/diseaseRecommendation',
            component: DiseaseRecommendation,
            meta: { auth: true },
        },
        {
            path: '/payment_of_treatment_report',
            component: PaymentOfTreatmentReport,
            meta: { auth: true },
        },
        {
            path: '/price_list_report',
            component: PriceListReport,
            meta: { auth: true },
        },
        {
            path: '/distribution_patient_by_ward',
            component: DistributionPatientsByWard,
            meta: { auth: true },
        },
        {
            path: '/purpose_of_treatment',
            component: PurposeOfTreatment,
            meta: { auth: true },
        },
        {
            path: '/disease_dynamic_report',
            component: DiseaseDynamicReport,
            meta: { auth: true },
        },
        {
            path: '/statement_of_paid_service',
            component: StatementOfPaidServices,
            meta: { auth: true },
        },
        {
            path: '/drug_recommendation',
            component: DrugRecommendation,
            meta: { auth: true },
        },
        {
            path: '/receipt_report',
            component: ReceiptReport,
            meta: { auth: true },
        },
        {
            path: '/diagram_ward',
            component: WardDiagram,
            meta: { auth: true },
        },
        {
            path: '/price_archive',
            component: PriceArchive,
            meta: { auth: true },
        },
        {
            path: '/accountant_diagram',
            component: AccountantDiagram,
            meta: { auth: true },
        },
        {
            path: '/service_drug_archive',
            component: ServiceAndDrugArchive,
            meta: { auth: true },
        },
        {
            path: '/dynamic_report',
            component: DynamicReport,
            meta: { auth: true },
        },
        {
            path: '/accountant_dynamic_report',
            component: AccountantDynamicReport,
            meta: { auth: true },
        },
        {
            path: '/perfomingTreatment',
            component: PerfomingTreatment,
            meta: { auth: true, title: 'Медицинские услуги'},
        },
        {
            path: '/app',
            component: App,
            meta: { title: 'Медицинские услуги'},
        }
    ],
    mode: "history"
})